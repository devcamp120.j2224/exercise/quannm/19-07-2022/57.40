package com.devcamp.j5740.j5740;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class J5740Application {

	public static void main(String[] args) {
		SpringApplication.run(J5740Application.class, args);
	}

}
