package com.devcamp.j5740.j5740.controller;

import com.devcamp.j5740.j5740.model.*;
import com.devcamp.j5740.j5740.service.PersonService;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;;

@RestController
public class PersonController {
    @CrossOrigin
    @GetMapping("/listPerson")

    public ArrayList<Person> getListPerson() {
        PersonService listPerson = new PersonService();
        return listPerson.getListAllPerson();
    }

    @CrossOrigin
    @GetMapping("/typePerson")
    public ArrayList<Person> getPersonFromType(@RequestParam int type) {
        ArrayList<Person> list = new ArrayList<>();
        if (type == 1) {
                list = new PersonService().getListStudent();
        }
        else if (type == 2) {
                list = new PersonService().getListWorker();
        }
        else if (type == 3) {
                list = new PersonService().getListProfessor();
        }
        else {
                list = new PersonService().getListAllPerson();
        }
        return list;
    }
}
