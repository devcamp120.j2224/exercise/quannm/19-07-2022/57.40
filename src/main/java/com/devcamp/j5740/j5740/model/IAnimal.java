package com.devcamp.j5740.j5740.model;

public interface IAnimal {
    public void animalSound();

    public void sleep();
}
