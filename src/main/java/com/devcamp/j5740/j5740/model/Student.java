package com.devcamp.j5740.j5740.model;

import java.util.ArrayList;

public class Student extends Person implements ISchool {
    private int studentId;
    private ArrayList<Subject> listSubject;

    public Student(int age, String gender, String name, Address address, ArrayList<Animal> listPet) {
        super(age, gender, name, address, listPet);
    }

    public Student(int age, String gender, String name, Address address, ArrayList<Animal> listPet, int studentId, ArrayList<Subject> listSubject) {
        super(age, gender, name, address, listPet);
        this.studentId = studentId;
        this.listSubject = listSubject;
    }

    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    public ArrayList<Subject> getListSubject() {
        return listSubject;
    }

    public void setListSubject(ArrayList<Subject> listSubject) {
        this.listSubject = listSubject;
    }

    @Override
    public void eat() {
        System.out.println("Student eating...");
    }

    public String doHomework() {
        return new String("Student do homework...");
    }

    @Override
    public void sleep() {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void gotoShop() {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void play() {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void gotoSchool() {
        // TODO Auto-generated method stub
        
    }
}
