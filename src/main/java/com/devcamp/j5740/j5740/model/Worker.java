package com.devcamp.j5740.j5740.model;

import java.util.ArrayList;

public class Worker extends Person {
    private int salary;

    public Worker(int age, String gender, String name, Address address, ArrayList<Animal> listPet) {
        super(age, gender, name, address, listPet);
    }

    public Worker(int age, String gender, String name, Address address, ArrayList<Animal> listPet, int salary) {
        super(age, gender, name, address, listPet);
        this.salary = salary;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    @Override
    public void eat() {
        System.out.println("Worker eating...");
    }

    public String working() {
        return new String("Worker is working...");
    }

    @Override
    public void sleep() {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void gotoShop() {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void play() {
        // TODO Auto-generated method stub
        
    }
}
